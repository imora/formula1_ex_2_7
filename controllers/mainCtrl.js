/* * * * * * * * * * * * * * * * * * * */     
/*      Main Controller     
/* * * * * * * * * * * * * * * * * * */ 

(function() {

    'use strict';
appFormula1.controller("mainCtrl", ['$scope', '$http','dataService',function($scope, $http, dataService){
    
    $scope.startYear=2005;
    $scope.endYear=2015;
    
    // $http.get("http://ergast.com/api/f1/2005/results/1.json?callback=json")
    // .then(function(response) {
    //     $scope.f1data[0] = response.data;
    // });
    
    
    
/* * * * * * * * * * * * * * * * * * * */     
/* Function to Load information per year         
/* * * * * * * * * * * * * * * * * * */  
    
    $scope.getData = function(){
        
        $scope.realData=[];
        var finishedRequests = 0;
        for( var year=$scope.startYear; year<$scope.endYear+1;year++){
            dataService.getData(year).then(function(dataResponse) {    
                dataResponse.data.MRData.RaceTable.season=parseInt(dataResponse.data.MRData.RaceTable.season);
                dataResponse.data.MRData.RaceTable.seasonWinnerDriver=$scope.getRaceWinner(  dataResponse.data.MRData.RaceTable.Races);
                /*Set Season winner for each year*/
                $scope.realData.push(dataResponse.data.MRData.RaceTable);
                finishedRequests++;      
        }).finally(function() {
            if(finishedRequests==($scope.endYear-$scope.startYear+1))
            {
                $scope.realData.sort( $scope.sortingSeason); 
                console.log(  $scope.realData);
            }
        });

        }        
    } 

    
/* * * * * * * * * * * * * * * * * * * * * */     
/* Function to determine Season Winner        
/* * * * * * * * * * * * * * * * * * * * */ 
    
    $scope.getRaceWinner =function(races){

        var seasonWinnersSum =[];
            $scope.result=[];

        for(var i=0; i<races.length; i++){
            $scope.result.push(races[i].Results[0].Driver.code);
            }

        for(var i=0; i<  $scope.result.length; i++){
            if(seasonWinnersSum.length==undefined || seasonWinnersSum.length==0){
                seasonWinnersSum.push( {id: $scope.result[i],count:1});           
            }else{   
                var auxLength=angular.copy(seasonWinnersSum.length); 
                for(var j=0 ;j<auxLength; j++){                         
                    if ($scope.result[i] == seasonWinnersSum[j].id){
                       seasonWinnersSum[j].count = seasonWinnersSum[j].count +1;
                    }else {
                          var insert=true;
                          for(var h=0 ;h<seasonWinnersSum.length; h++ ){ 
                              if($scope.result[i] == seasonWinnersSum[h].id)
                                insert=false;                              
                          }
                           if(insert)
                                seasonWinnersSum.push( {id: $scope.result[i], count:1}); 
                           }        
                    }       
                }           
        }
        seasonWinnersSum.sort($scope.sorting);
        return seasonWinnersSum[seasonWinnersSum.length-1].id;
    }
    
/* * * * * * * * * * * * * * * * * * * * * */     
/* Functions to sort arrays        
/* * * * * * * * * * * * * * * * * * * * */ 
    
    
    $scope.sorting = function(x,y){  
        return x.count - y.count ;
    }
    $scope.sortingSeason = function(x,y){  
        return x.season - y.season ;
    }
  
/* * * * * * * * * * * * * * * * * * * * * */     
/* Functions to show details of each season      
/* * * * * * * * * * * * * * * * * * * * */ 
    
    $scope.showDetails = function(index){ 
        var detail = document.getElementById("detailsChamp_container_"+index);
        console.log(detail);
        if (!detail.style.display || detail.style.display === "none") {
            detail.style.display = "block";
        } else {
            detail.style.display = "none";
        } 
        
    }
  
    
/* * * * * * * * * * * * * * * * * * * */     
/* Functionality of the rolling car   
/* * * * * * * * * * * * * * * * * * */  

     angular.element(window).on('scroll', function() {   
        $("#f1Car").css({marginTop:this.pageYOffset*($('#carContainer').height()/$(document).height())*.5});

    });

}]);

})();

