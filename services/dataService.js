
/* * * * * * * * * * * * * * * * * * * */     
/* Service to get Ergast API information 
/* * * * * * * * * * * * * * * * * * */  
(function() {

    'use strict';

appFormula1.service('dataService', [
        '$http',
        function ($http) {


        this.getData = function(year) {

        var tmp= $http({
            method: 'GET',
            url: 'http://ergast.com/api/f1/'+year+'/results/1.json',
            params: 'callback=json',
         });
        return tmp;
     }


}]);

})();
