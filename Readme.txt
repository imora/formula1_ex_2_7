How to run the project: 
You have to open the index.html file  to see the main page, 
then click on "Speed your car" button to list the Championships from 2005 to 2015 
(the list could be displayed at the beginning easily, but I decided to add a button), 
scrolling the page down you can see all the championships,
finally, clicking on the desired year, details from that season will be displayed.



What have I done?


First get the data from the API for each year in json and accomodate it in array.

Determine the winner for each year using auxiliary arrays and loops to count the winning rounds per driver.
then sort the array to get the biggest amount of winnings, after having this value, I added it as an attribute to my iterable main array (this to avoid the creation of another array or object and to keep the information in the same object so I can read it easily in the view) in order to highlight the winner. 

Using Angular tags such as ng-repeat and ng-if I was able to iterate the object obtained after all the treatment in arrays in javascript functions.

I added an animation, a funny car that guides the user till the end of the list, getting the scroll event and using a jquery function to manipulate the  car position in the screen.

As requested when clicking on an item (title), the list of winners of all the Grand Prixes are shown, this using css :hover

