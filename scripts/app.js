var appFormula1 = angular.module("appFormula1", ['ngRoute']);

appFormula1.config( function($routeProvider) {
    $routeProvider.
      when('/formula1_winners', {
        templateUrl: 'views/main.html',
        controller: 'mainCtrl'
      });
  });
